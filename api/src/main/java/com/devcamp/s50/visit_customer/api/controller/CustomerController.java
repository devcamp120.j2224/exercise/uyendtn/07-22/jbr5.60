package com.devcamp.s50.visit_customer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.visit_customer.api.model.Customer;
import com.devcamp.s50.visit_customer.api.service.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin(value="*",maxAge = -1)
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public List<Customer> getCustomer() {
        List<Customer> customerList = customerService.getCustomerList();
        return customerList;
    }
}
