package com.devcamp.s50.visit_customer.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.visit_customer.api.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("OHMM", true, "GOLD");
    Customer customer2 = new Customer("XTRA", true, "SILVER");
    Customer customer3 = new Customer("UNIVERSAL", true, "COPPER");

    public List<Customer> getCustomerList() {
        List<Customer> customerList = new ArrayList<Customer>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList;
    }
}
