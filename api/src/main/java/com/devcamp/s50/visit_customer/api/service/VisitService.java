package com.devcamp.s50.visit_customer.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.visit_customer.api.model.Visit;

@Service
public class VisitService {
    @Autowired
    private CustomerService customerService;
    
    public List<Visit> getVisitList() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = format.parse("2022-07-15");
        Date date2 = format.parse("2022-06-15");
        Date date3 = format.parse("2022-05-15");

        Visit visit1 = new Visit(customerService.customer1, date1);
        Visit visit2 = new Visit(customerService.customer2, date2);
        Visit visit3 = new Visit(customerService.customer3, date3);

        //tạo arrayList visit và add visit vào
        List<Visit> visitList = new ArrayList<Visit>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;
    }
}
