package com.devcamp.s50.visit_customer.api.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.visit_customer.api.model.Visit;
import com.devcamp.s50.visit_customer.api.service.VisitService;

@RestController
@RequestMapping("/")
@CrossOrigin(value="*",maxAge = -1)
public class VisitController {
    @Autowired
    private VisitService visitService;

    @GetMapping("/visits")
    public List<Visit> getVisit() throws ParseException {
        List<Visit> visitList = visitService.getVisitList();
        return visitList;
    }
}